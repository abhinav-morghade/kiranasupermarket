package com.kiranasupermarket.services.security;

public interface JWTAuthenticationDetails {

	public JWTClaimVO retrieveUserDetails(String authToken);
	
}
