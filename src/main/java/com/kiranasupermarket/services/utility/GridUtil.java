package com.kiranasupermarket.services.utility;

import org.springframework.stereotype.Component;

/**
 * @author shivtej.sumbe
 *
 */
@Component
public class GridUtil {
	
	 /**
	 * @param currentPage
	 * @param totalRowsToFetch
	 * @return Integer startIndex
	 */
	public Integer calculateStartIndexForList(final Integer currentPage, final Integer totalRowsToFetch) {
	  Integer startIndex = (currentPage * totalRowsToFetch) - totalRowsToFetch;
	  return startIndex;
	 }
}
