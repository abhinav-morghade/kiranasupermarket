package com.kiranasupermarket.services.utility;

/**
 * @author antara.datta
 *
 */
public class ApplicationUtil {
	
	public static boolean isStringNullOrEmpty (String value) {
		boolean isStringNullOrEmpty =true;
		if (value != null && value.trim().length() > 0) {
			isStringNullOrEmpty = false;
		}
		return isStringNullOrEmpty;
	}

}
