package com.kiranasupermarket.services.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import com.kiranasupermarket.services.entity.User;

/**
 * @author abhinav
 *
 */
public interface UserRepository extends CrudRepository<User, Long> {
	
	public User findByUsername(@Param("username") String username);
	
	public User findById(@Param("id") String username);
	
}
