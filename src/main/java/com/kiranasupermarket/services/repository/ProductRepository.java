package com.kiranasupermarket.services.repository;

import org.springframework.data.repository.CrudRepository;
import com.kiranasupermarket.services.entity.Product;

/**
 * @author abhinav
 *
 */
public interface ProductRepository extends CrudRepository<Product, Long> {

}
