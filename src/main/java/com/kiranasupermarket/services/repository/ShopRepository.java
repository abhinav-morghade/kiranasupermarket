package com.kiranasupermarket.services.repository;

import org.springframework.data.repository.CrudRepository;
import com.kiranasupermarket.services.entity.Shop;

public interface ShopRepository  extends CrudRepository<Shop, Long> {

}
