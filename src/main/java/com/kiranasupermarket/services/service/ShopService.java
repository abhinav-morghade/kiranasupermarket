package com.kiranasupermarket.services.service;

import java.util.List;
import com.kiranasupermarket.services.entity.Shop;

public interface ShopService {

	public void createShop(Shop shop);

	public List<Shop> getShop();

	public Shop findById(long id);

	public Shop update(Shop shop);

	public void deleteShopById(long id);

}
