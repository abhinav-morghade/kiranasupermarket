package com.kiranasupermarket.services.service;

import java.util.List;

import com.kiranasupermarket.services.dto.ResponseVO;
import com.kiranasupermarket.services.dto.UserLoginVO;
import com.kiranasupermarket.services.entity.User;
import com.kiranasupermarket.services.utility.exception.ApplicationException;

public interface UserService {

    public UserLoginVO authenticateUser(UserLoginVO userLoginDetails) throws ApplicationException;

    /**
     * Method for findByuserName
     * @param userName
     * @return
     */
    public User findByuserName(String userName);
    
    public ResponseVO createUser(User user);

	public User getUserByID(Long id);

	public List<User> findAll();
}
