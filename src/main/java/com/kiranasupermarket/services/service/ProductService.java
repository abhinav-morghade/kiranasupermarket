package com.kiranasupermarket.services.service;

import java.util.List;
import com.kiranasupermarket.services.entity.Product;

public interface ProductService {

	public void createProduct(Product product);

	public List<Product> getProduct();

	public Product findById(long id);

	public Product update(Product product);

	public void deleteProductById(long id);

}
