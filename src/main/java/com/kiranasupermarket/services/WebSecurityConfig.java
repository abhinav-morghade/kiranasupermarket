package com.kiranasupermarket.services;

import java.util.Arrays;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.ProviderManager;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import com.kiranasupermarket.services.security.JwtAuthenticationEntryPoint;
import com.kiranasupermarket.services.security.JwtAuthenticationProvider;
import com.kiranasupermarket.services.security.JwtAuthenticationSuccessHandler;
import com.kiranasupermarket.services.security.JwtAuthenticationTokenFilter;

@Configuration
@EnableWebSecurity
@EnableAutoConfiguration
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

	@Autowired
	private JwtAuthenticationEntryPoint unauthorizedHandler;

	@Autowired
	private JwtAuthenticationProvider authenticationProvider;

	@Bean
	@Override
	public AuthenticationManager authenticationManager() throws Exception {

		return new ProviderManager(Arrays.asList(authenticationProvider));
	}

	public JwtAuthenticationTokenFilter authenticationTokenFilterBean() throws Exception {
		JwtAuthenticationTokenFilter authenticationTokenFilter = new JwtAuthenticationTokenFilter();
		/*
		 * Change to the url which are to be filtered
		 */
		authenticationTokenFilter.setFilterProcessesUrl("/*kiranasupermarket/**");
		//authenticationTokenFilter.setFilterProcessesUrl("/*camps/**");
		authenticationTokenFilter.setAuthenticationManager(authenticationManager());
		authenticationTokenFilter.setAuthenticationSuccessHandler(new JwtAuthenticationSuccessHandler());
		return authenticationTokenFilter;
	}

	@Override
	public void configure(WebSecurity web) throws Exception {
		web.ignoring().antMatchers("/swagger-ui/index.html","/v1/user/**","/configuration/ui","/swagger-resources","/v2/api-docs/**","/v1/v2/api-docs","/swagger-ui.html/**","/webjars/**").antMatchers("/v2/api-docs").antMatchers("/v1/v2/api-docs");
		//web.ignoring().antMatchers("/user/**","/v2/api-docs/**");
		
	}

	@Override
	protected void configure(HttpSecurity httpSecurity) throws Exception {
		httpSecurity
				// we don't need CSRF because our token is invulnerable
				.csrf().disable()
				// All urls must be authenticated (filter for token always fires
				// (/**)

                .authorizeRequests().antMatchers(HttpMethod.OPTIONS, "/*/**").permitAll().antMatchers().permitAll()
                .and()
                .authorizeRequests()
                .antMatchers("/api/user/login","/api/user/createUser", "/api/user/getUserByID", "/api/user/getAllUsers").permitAll()
                .antMatchers(HttpMethod.GET, "/api-docs/**", "/swagger-ui/**").permitAll()
				.anyRequest().authenticated().and()
				.exceptionHandling().authenticationEntryPoint(unauthorizedHandler).and()
				// don't create session
				.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS); // .and()
		// Custom JWT based security filter
		httpSecurity.addFilterBefore(authenticationTokenFilterBean(), UsernamePasswordAuthenticationFilter.class);

		// disable page caching
		httpSecurity.headers().cacheControl();
	}
}