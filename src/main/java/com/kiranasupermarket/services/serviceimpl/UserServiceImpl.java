package com.kiranasupermarket.services.serviceimpl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import com.kiranasupermarket.services.dto.ResponseVO;
import com.kiranasupermarket.services.dto.UserLoginVO;
import com.kiranasupermarket.services.entity.User;
import com.kiranasupermarket.services.repository.UserRepository;
import com.kiranasupermarket.services.security.IJWTBuilder;
import com.kiranasupermarket.services.security.JWTClaimVO;
import com.kiranasupermarket.services.service.UserService;
import com.kiranasupermarket.services.utility.ApplicationLogger;
import com.kiranasupermarket.services.utility.constants.ApplicationConstants;
import com.kiranasupermarket.services.utility.exception.ApplicationException;
import com.kiranasupermarket.services.utility.exception.EncryptionUtil;
import com.kiranasupermarket.services.utility.exception.ErrorCode;

/**
 * @author abhinav
 *
 */
public class UserServiceImpl implements UserService {

    /**
     * Instance for jwtBuilder	
     */
    @Autowired
    private IJWTBuilder jwtBuilder;

    /**
     * Instance for userRepository
     */
    @Autowired
    private UserRepository userRepository;

    /**
     * instance for ApplicationLogger
     */
    private static final ApplicationLogger LOGGER = new ApplicationLogger(UserServiceImpl.class);


    /**
     * Method for find user by Name
     */
    @Override
    public User findByuserName(String userName) {
        User user = userRepository.findByUsername(userName);
        return user;
    }

    /**
     * Method for authenticate User
     */
    @Override
    public UserLoginVO authenticateUser(UserLoginVO userLoginDetails) throws ApplicationException {
        LOGGER.info("Authenticate user into Database");
        User user = userRepository.findByUsername(userLoginDetails.getUsername());
        if (user != null) {
            LOGGER.info("User with given credentials returned from database");
            String decryptPassword = user.getPassword();
            LOGGER.info("Authentication user password");
            if (decryptPassword.equals(userLoginDetails.getPassword())) {
                UserLoginVO userLoginVO = new UserLoginVO();
                userLoginVO.setId(user.getId());
                userLoginVO.setUsername(user.getUsername());
                userLoginVO.setRole(user.getRole());
                JWTClaimVO jwtClaimVO = createClaim(userLoginVO);
                Date today = new Date();
                Date expirationDate = new Date(today.getTime() + (1000 * 60 * 60 * 24));
                userLoginVO.setToken(jwtBuilder.createJWT(jwtClaimVO, expirationDate));
                userLoginVO.setMessage("Success");
                return userLoginVO;
            } else {
                LOGGER.info("Password incorrect");
                throw new ApplicationException(ApplicationConstants.INVALID_PASSWORD,
                        401, HttpStatus.UNAUTHORIZED);
            }

        } else {
            LOGGER.info("Username incorrect");
            throw new ApplicationException(ApplicationConstants.USER_NOT_FOUND, 401,
                    HttpStatus.UNAUTHORIZED);
        }
    }

    
    @Override
	@Transactional
	public ResponseVO createUser(User user) {
		ResponseVO responseVO = new ResponseVO();
		if (user.getUsername() != null) {
			LOGGER.info("Recieving user for creating user.");
			LOGGER.info("Calling findByUsername method of userRepository to find user by name.");
			User existingUser = userRepository.findByUsername(user.getUsername());
			if (user.getUsername() != null) {
				if (existingUser != null) {
					throw new ApplicationException("User Already Exists", ErrorCode.USER_ALREADY_EXIST.getCodeId(),
							HttpStatus.INTERNAL_SERVER_ERROR);
				} else {
					LOGGER.info("Calling save method of userRepository to find create user.");
					String encryptPassword = EncryptionUtil.encryptData(user.getPassword());
					user.setPassword(encryptPassword);
					User response = userRepository.save(user);
					if (response == null) {
						throw new ApplicationException("Unexpected Error", ErrorCode.UNEXPECTED_ERROR.getCodeId(),
								HttpStatus.INTERNAL_SERVER_ERROR);
					} else {
						LOGGER.info("Received response from save method of userRepository.");
						responseVO.setStatusCode("200");
						responseVO.setMessage("Success");
					}
				}
			}
		} else {
			throw new ApplicationException("Please provide user details", ErrorCode.UNEXPECTED_ERROR.getCodeId(),
					HttpStatus.NO_CONTENT);
		}
		return responseVO;
	}

	@Override
	@Transactional
	public User getUserByID(Long id) {
		LOGGER.info("Recieving user id for getting user by id.");
		User userRes = new User();
		if (id != null) {
			LOGGER.info("Calling getById method of userRepository to find user by id.");
			User user = userRepository.findById(id).get();
			if (user == null) {
				throw new ApplicationException("Provided User Not Present", ErrorCode.UNEXPECTED_ERROR.getCodeId(),
						HttpStatus.INTERNAL_SERVER_ERROR);
			} else {
				LOGGER.info("Received response from getById method of userRepository.");
				return user;
			}
		}
		return userRes;
	}

    
    /**
     * Method for create Claim
     */
    private JWTClaimVO createClaim(UserLoginVO user) {

        JWTClaimVO jwtClaimVO = new JWTClaimVO();
        jwtClaimVO.setUsername(user.getUsername());
        jwtClaimVO.setRoles(user.getRole());
        jwtClaimVO.setUserPk(user.getId().toString());
        return jwtClaimVO;

    }

	@Override
	public List<User> findAll() {
		List<User> users = (List<User>) userRepository.findAll();
		List<User> userList = new ArrayList<>();
		for (User user : users) {
			User userObj = new User();
			userObj.setId(user.getId());
			userObj.setFirstName(user.getFirstName());
			userObj.setLastName(user.getLastName());
			userObj.setRole(user.getRole());
			userObj.setUsername(user.getUsername());
			userObj.setImage(user.getImage());
			userList.add(userObj);
		}
		return userList;
	}

}
