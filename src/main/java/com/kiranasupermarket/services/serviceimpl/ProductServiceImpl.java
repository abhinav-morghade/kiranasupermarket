package com.kiranasupermarket.services.serviceimpl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import com.kiranasupermarket.services.entity.Product;
import com.kiranasupermarket.services.repository.ProductRepository;
import com.kiranasupermarket.services.service.ProductService;

/**
 * @author abhinav
 *
 */
public class ProductServiceImpl implements ProductService {

	/**
	 * Instance for productRepository
	 */
	@Autowired
	ProductRepository productRepository;

	@Override
	public void createProduct(Product product) {
		productRepository.save(product);
	}

	@Override
	public List<Product> getProduct() {
		return (List<Product>) productRepository.findAll();
	}

	@Override
	public Product findById(long id) {
		return productRepository.findById(id).get();
	}

	@Override
	public Product update(Product product) {
		Product productIns = new Product();
		if (product != null) {
			Product productObj = productRepository.findById(product.getId()).get();
			if (productObj != null) {
				productIns = productRepository.save(product);
			} else {
				return null;
			}
		}
		return productIns;
	}

	@Override
	public void deleteProductById(long id) {
		productRepository.deleteById(id);
	}

}
