package com.kiranasupermarket.services.serviceimpl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import com.kiranasupermarket.services.entity.Shop;
import com.kiranasupermarket.services.repository.ShopRepository;
import com.kiranasupermarket.services.service.ShopService;

/**
 * @author abhinav
 *
 */
public class ShopServiceImpl implements ShopService {

	/**
	 * Instance for shopRepository
	 */
	@Autowired
	ShopRepository shopRepository;

	@Override
	public void createShop(Shop shop) {
		shopRepository.save(shop);
	}

	@Override
	public List<Shop> getShop() {
		return (List<Shop>) shopRepository.findAll();
	}

	@Override
	public Shop findById(long id) {
		return shopRepository.findById(id).get();
	}

	@Override
	public Shop update(Shop shop) {
		Shop shopIns = new Shop();
		if (shop != null) {
			Shop shopObj = shopRepository.findById(shop.getId()).get();
			if (shopObj != null) {
				shopIns = shopRepository.save(shop);
			} else {
				return null;
			}
		}
		return shopIns;
	}

	@Override
	public void deleteShopById(long id) {
		shopRepository.deleteById(id);
	}

}
