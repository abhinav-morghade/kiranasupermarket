package com.kiranasupermarket.services.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;
import com.kiranasupermarket.services.entity.Shop;
import com.kiranasupermarket.services.service.ShopService;

@RestController
@RequestMapping(value = { "/shop" })
public class ShopController {

	@Autowired
	ShopService shopService;

	@GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Shop> getShopById(@PathVariable("id") long id) {
		Shop shop = shopService.findById(id);
		if (shop == null) {
			return new ResponseEntity<Shop>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<Shop>(shop, HttpStatus.OK);
	}

	@PostMapping(value = "/create", headers = "Accept=application/json")
	public ResponseEntity<Void> createShop(@RequestBody Shop shop, UriComponentsBuilder ucBuilder) {
		shopService.createShop(shop);
		HttpHeaders headers = new HttpHeaders();
		headers.setLocation(ucBuilder.path("/shop/{id}").buildAndExpand(shop.getId()).toUri());
		return new ResponseEntity<Void>(headers, HttpStatus.CREATED);
	}

	@GetMapping(value = "/get", headers = "Accept=application/json")
	public List<Shop> getAllShop() {
		List<Shop> tasks = shopService.getShop();
		return tasks;

	}

	@PutMapping(value = "/update", headers = "Accept=application/json")
	public ResponseEntity<String> updateShop(@RequestBody Shop currentshop) {
		Shop shop = shopService.findById(currentshop.getId());
		if (shop == null) {
			return new ResponseEntity<String>(HttpStatus.NOT_FOUND);
		}
		shopService.update(currentshop);
		return new ResponseEntity<String>(HttpStatus.OK);
	}

	@DeleteMapping(value = "/{id}", headers = "Accept=application/json")
	public ResponseEntity<Shop> deleteshop(@PathVariable("id") long id) {
		Shop shop = shopService.findById(id);
		if (shop == null) {
			return new ResponseEntity<Shop>(HttpStatus.NOT_FOUND);
		}
		shopService.deleteShopById(id);
		return new ResponseEntity<Shop>(HttpStatus.NO_CONTENT);
	}
}
