package com.kiranasupermarket.services.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.kiranasupermarket.services.dto.ResponseVO;
import com.kiranasupermarket.services.dto.UserLoginVO;
import com.kiranasupermarket.services.entity.User;
import com.kiranasupermarket.services.security.JWTAuthenticationDetails;
import com.kiranasupermarket.services.service.UserService;
import com.kiranasupermarket.services.utility.ApplicationLogger;

@RestController
public class UserController {

	/**
	 * Method for userService
	 */
	@Autowired
	private UserService userService;
	
	/**
	 * Object of JWTAunthenticationDetails
	 */
	@Autowired
	private JWTAuthenticationDetails jWTAuthenticationDetails;

	private static final ApplicationLogger LOGGER = new ApplicationLogger(UserController.class);

	
	/**
	 * Method for Authentication of user
	 * @param userData
	 * @return responseEntity
	 */
	@RequestMapping(value = "/api/user/login", method = RequestMethod.POST)
	public ResponseEntity<?> authenticateUser(@RequestBody final UserLoginVO userData) {
		LOGGER.info("Recieving user ceredentials for user login");
		UserLoginVO userLoginVO = userService.authenticateUser(userData);
		if (userLoginVO != null) {
			return new ResponseEntity<UserLoginVO>(userLoginVO, HttpStatus.OK);
		} else {
			ResponseVO responseVO = new ResponseVO();
			responseVO.setMessage("failure");
			responseVO.setStatusCode("400");
			return new ResponseEntity<ResponseVO>(responseVO, HttpStatus.NOT_FOUND);
		}
	}

	@RequestMapping(value = "/api/user/createUser", method = RequestMethod.POST, produces = "application/json")
	public ResponseEntity<?> createUser(@RequestBody User user) {
		LOGGER.info("Recieving user for creating user.");
		LOGGER.info("Calling createUser method of DqatService to create user.");
		ResponseVO responseVO = userService.createUser(user);
		LOGGER.info("Received response from createUser method of DqatService.");
		return new ResponseEntity<ResponseVO>(responseVO, HttpStatus.OK);
	}

	@RequestMapping(value = "/api/user/getUserByID", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<?> getUserByID(@RequestParam(value = "userId") Long userId) {
		LOGGER.info("Recieving userId to get user by id.");
		User user = new User();
		LOGGER.info("Calling getUserByID method of DqatService to get user by id.");
		user = userService.getUserByID(userId);
		LOGGER.info("Received response from getUserByID method of DqatService.");
		return new ResponseEntity<User>(user, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/api/user/getAllUsers", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<?> getAllUsers() {
		List<User> user = userService.findAll();
		return new ResponseEntity<List<User>>(user, HttpStatus.OK);
	}
	
}
