package com.kiranasupermarket.services.entity;

import java.util.Arrays;

import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;

@Entity
public class Shop {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private String shopName;
	private String shopKeeperName;
	private String shopDescription;
	private Double latitude;
	private Double longitute;
	private String city;
	private String address;
	private Integer pincode;
	private Integer mobileNo;
	@Lob
	@Basic(fetch = FetchType.LAZY)
	private byte[] shopImage;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getShopName() {
		return shopName;
	}

	public void setShopName(String shopName) {
		this.shopName = shopName;
	}

	public String getShopKeeperName() {
		return shopKeeperName;
	}

	public void setShopKeeperName(String shopKeeperName) {
		this.shopKeeperName = shopKeeperName;
	}

	public String getShopDescription() {
		return shopDescription;
	}

	public void setShopDescription(String shopDescription) {
		this.shopDescription = shopDescription;
	}

	public Double getLatitude() {
		return latitude;
	}

	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}

	public Double getLongitute() {
		return longitute;
	}

	public void setLongitute(Double longitute) {
		this.longitute = longitute;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Integer getPincode() {
		return pincode;
	}

	public void setPincode(Integer pincode) {
		this.pincode = pincode;
	}

	public Integer getMobileNo() {
		return mobileNo;
	}

	public void setMobileNo(Integer mobileNo) {
		this.mobileNo = mobileNo;
	}

	public byte[] getShopImage() {
		return shopImage;
	}

	public void setShopImage(byte[] shopImage) {
		this.shopImage = shopImage;
	}

	@Override
	public String toString() {
		return "Shop [id=" + id + ", shopName=" + shopName + ", shopKeeperName=" + shopKeeperName + ", shopDescription="
				+ shopDescription + ", latitude=" + latitude + ", longitute=" + longitute + ", city=" + city
				+ ", address=" + address + ", pincode=" + pincode + ", mobileNo=" + mobileNo + ", shopImage="
				+ Arrays.toString(shopImage) + "]";
	}

}
