package com.kiranasupermarket.services;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.PropertySource;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import com.kiranasupermarket.services.service.ProductService;
import com.kiranasupermarket.services.service.ShopService;
import com.kiranasupermarket.services.service.UserService;
import com.kiranasupermarket.services.serviceimpl.ProductServiceImpl;
import com.kiranasupermarket.services.serviceimpl.ShopServiceImpl;
import com.kiranasupermarket.services.serviceimpl.UserServiceImpl;

@CrossOrigin(origins = "*")
@SpringBootApplication
@EnableConfigurationProperties
@ComponentScan(basePackages = "com.kiranasupermarket.services")
@PropertySource({ "classpath:jwt.properties" })
public class KiranasupermarketApplication extends WebMvcConfigurerAdapter {

	public static void main(String[] args) {
		SpringApplication.run(KiranasupermarketApplication.class, args);
	}

	@Bean
	public UserService userService() {
		return new UserServiceImpl();
	}
	
	@Bean
	public ShopService shopService() {
		return new ShopServiceImpl();
	}
	
	@Bean
	public ProductService productService() {
		return new ProductServiceImpl();
	}
	
	@Override
	public void addCorsMappings(CorsRegistry registry) {
		registry.addMapping("/**").allowedOrigins("*").allowedMethods("GET", "POST", "PUT", "DELETE", "PATCH");
	}
}
